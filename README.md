windows.yaml - образец ansible playbook  для создания виндовс машины в среде hyper с использованием, подготовленного  box  vagrant.
vagrantfile - шаблон ,
secrets.yaml - шифрованные данные,
vagrant.yaml - файл с переменными для шаблона vagrant
Характеристики создаваемого кластера машин задаются в vagrant.yaml:
NUMBER_VM - кол-во машин
NAME_VM - имя машины (в hyper-v)
NUMBER_CPU - кол-во ядер CPU
NUMBER_RAM - объем ОЗУ
Кроме этого в шаблоне vagrantfile заданы значения по умолчанию для этих параметров (применяются когда переменные не опеределены)